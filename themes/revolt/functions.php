<?php
/**
 * Ekko functions file
 *
 * @package ekko
 * by KeyDesign
 */

 add_action( 'wp_enqueue_scripts', 'kd_enqueue_parent_theme_style', 5 );
 if ( ! function_exists( 'kd_enqueue_parent_theme_style' ) ) {
     function kd_enqueue_parent_theme_style() {
         wp_enqueue_style( 'bootstrap' );
         wp_enqueue_style( 'keydesign-style', get_template_directory_uri() . '/style.css', array( 'bootstrap' ) );
         wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array('keydesign-style') );
     }
 }

 add_action( 'after_setup_theme', 'kd_child_theme_setup' );
 if ( ! function_exists( 'kd_child_theme_setup' ) ) {
     function kd_child_theme_setup() {
         load_child_theme_textdomain( 'ekko', get_stylesheet_directory() . '/languages' );
     }
 }

 // -------------------------------------
 // Edit below this line
 // -------------------------------------
 require_once 'elements/elements.php';

 // Fix for aws
function as3cf_filter_get_post_metadata( $metadata, $object_id, $meta_key, $single ) {
    $meta_filter = array('_wpb_shortcodes_custom_css', '_wpb_post_custom_css');
    if ( isset( $meta_key ) && $single && in_array($meta_key, $meta_filter)) {
        remove_filter( 'get_post_metadata', 'as3cf_filter_get_post_metadata', 100 );
        $metadata = get_post_meta( $object_id, $meta_key, $single );
        add_filter('get_post_metadata', 'as3cf_filter_get_post_metadata', 100, 4);
        $metadata = apply_filters( 'as3cf_filter_post_local_to_s3', $metadata );
    }
    return $metadata;
}
add_filter( 'get_post_metadata', 'as3cf_filter_get_post_metadata', 100, 4 );

add_action ('wp_footer', 'add_linkedIn_footer');
function add_linkedIn_footer() {
    ?>	
        <script type="text/javascript"> _linkedin_partner_id = "3914506"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script>
        <script type="text/javascript"> (function(l) { if (!l){window.lintrk = function(a,b){window.lintrk.q.push([a,b])}; window.lintrk.q=[]} var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(window.lintrk); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=3914506&fmt=gif" /> </noscript>
    <?php
}